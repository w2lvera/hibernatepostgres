CREATE SEQUENCE autos_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 18
  CACHE 1;


CREATE SEQUENCE users_sequence
  INCREMENT 1
  MINVALUE 50
  MAXVALUE 2000
  START 66
  CACHE 1;


CREATE TABLE users
(
  name character varying(50),
  age integer,
  id integer NOT NULL DEFAULT nextval('users_sequence'::regclass),
  CONSTRAINT pkey PRIMARY KEY (id )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO "user";

CREATE TABLE autos
(
  model character varying(50),
  color character varying(50),
  user_id integer,
  id integer NOT NULL DEFAULT nextval('autos_sequence'::regclass),
  CONSTRAINT aotus_pkey PRIMARY KEY (id ),
  CONSTRAINT users_autors_id_fk FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE autos
  OWNER TO "user";
