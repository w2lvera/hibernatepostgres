/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.hibernate1.sevices;

/**
 *
 * @author Wera
 */


import com.vera.hibernate1.dao.UserDao;
import com.vera.hibernate1.dao.UserDaoImp;
import com.vera.hibernate1.entity.Auto;
import com.vera.hibernate1.entity.User;
import java.util.List;

public class UserService {

    private UserDao usersDao = new UserDaoImp();

    public UserService() {
    }

    public User findUser(int id) {
        return usersDao.findById(id);
    }

    public void saveUser(User user) {
        usersDao.save(user);
    }

    public void deleteUser(User user) {
        usersDao.delete(user);
    }

    public void updateUser(User user) {
        usersDao.update(user);
    }

    public List<User> findAllUsers() {
        return usersDao.findAll();
    }
    public List<Auto> findAllAutos(User user) {
        
        return usersDao.findAllAutos(user);
    }
    public Auto findAutoById(int id) {
        return usersDao.findAutoById(id);
    }


}