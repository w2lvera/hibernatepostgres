/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.hibernate1.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import com.vera.hibernate1.utils.HibernateSessionFactoryUtil;
import com.vera.hibernate1.entity.Auto;
import com.vera.hibernate1.entity.User;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.query.Query;

/**
 *
 * @author Wera
 */
public class UserDaoImp implements UserDao{

    @Override
    public User findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(User.class, id);
    }

    @Override
    public void update(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(user);
        tx1.commit();
        session.close();
    }

    @Override
    public void save(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(user);
        tx1.commit();
        session.close();
    }

    @Override
    public void delete(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(user);
        tx1.commit();
        session.close();
    }

    @Override
    public Auto findAutoById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Auto.class, id);
    }

    @Override
    public List<User> findAll() {
        List<User> users = (List<User>)  
                HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From User").list();
        return users;
    }

    @Override
    public List<Auto> findAllAutos(User user) {
            Session session=null;
            List<Auto> autos = null;//new List<Auto>(); 
            try{
            session = HibernateSessionFactoryUtil.getSessionFactory().openSession();//.getCurrentSession();
            session.beginTransaction();
            int user_id = user.getId();
            Query query = session.createQuery(
          //" select  a.model, a.color  "
                  " select  a "
              + " from Auto  a INNER JOIN User as u on a.user = u.id"
              + " where a.user =  " + user_id     );
                 //   .setParameter(1,user_id);
            autos = (List<Auto>)query.list();
            session.getTransaction().commit();

    } finally {
      if (session != null && session.isOpen()) {
        session.close();
      }
      
    }
    return autos;
            
    }  
    
}
