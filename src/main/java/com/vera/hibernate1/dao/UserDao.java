/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.hibernate1.dao;

import com.vera.hibernate1.entity.Auto;
import com.vera.hibernate1.entity.User;
import java.util.List;

/**
 *
 * @author Wera
 */
public interface UserDao {
    User findById(int id);
    void update(User user);
    void save(User user);
    void delete(User user);
    Auto findAutoById(int id);
    List<User> findAll();
    List<Auto> findAllAutos(User user) ;
    
}
