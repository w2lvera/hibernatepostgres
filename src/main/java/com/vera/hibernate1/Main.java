package com.vera.hibernate1;

import com.vera.hibernate1.entity.Auto;
import com.vera.hibernate1.entity.User;
import com.vera.hibernate1.sevices.UserService;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Wera
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = new User("Yury",56);
        userService.saveUser(user);
        
        Auto ferrari = new Auto("Ferrari", "red");
        ferrari.setUser(user);
        user.addAuto(ferrari);
        
        Auto ford = new Auto("Ford", "black");
        ford.setUser(user);
        user.addAuto(ford);
        
        Auto citroen = new Auto("c3Picaso", "white");
        citroen.setUser(user);
        user.addAuto(citroen);
        userService.updateUser(user);
//        user.setName("Sasha");
//        userService.updateUser(user);
//        userService.deleteUser(user);
        
//        User user1 = new User();
//        user1 = userService.findUser(54);
//       
        
//        System.out.println(user);
//        ArrayList<User> list =(ArrayList) userService.findAllUsers();
//        for(User x:list)
//            System.out.println(x);
        List<Auto> listAuto = userService.findAllAutos(user);//user.getAutos();
       
        for(Auto x:listAuto)
            System.out.println(x);
    }
    
}
